//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/orders');

const auth = require('../auth');

// destructure verify from auth
const {verify, verifyAdmin, verifyUser} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

//[SECTION] [POST] Routes
//let our registered users order
route.post('/order', verify, verifyUser, controller.order)

//get logged user's orders
route.get('/getOrders', verify, verifyUser, controller.getOrders)

route.get('/getAllOrders', verify, verifyAdmin, controller.getAllOrders)


//[SECTION] Export Route System
module.exports = route;
