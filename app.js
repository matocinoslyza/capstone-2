// [SECTION] Packanges and Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv");
const productRoutes = require('./routes/products');
const userRoutes = require('./routes/users');
const orderRoutes = require('./routes/orders');

// [SECTION] Server Setup
const app = express();
dotenv.config();
app.use(cors())
app.use(express.json());
const secret = process.env.CONNECTION_STRING;
const port = process.env.PORT || 4000;

// [SECTION] Application Routes
app.use('/products', productRoutes);
app.use('/users', userRoutes);
app.use('/orders', orderRoutes);

// [SECTION] Database Connect
mongoose.connect(secret)
let connectionStatus = mongoose.connection;
connectionStatus.on('open', () => console.log('Database is connected'));

// [SECTION] Gateway Response
app.get('/', (req, res) => {
    res.send(`Lyza's Ecommerce API Capstone!`);
});

app.listen(port, () => console.log(`Server is running on port ${port}`));
