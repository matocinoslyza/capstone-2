//[SECTION] Dependencies and Modules
const auth = require("../auth");
const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require("bcrypt");
const dotenv = require("dotenv"); 

// [SECTION] Environment Setup
dotenv.config();
const salt1 = 10; 
const salt2 = 10; 

// [SECTION] Functionalities [CREATE]
module.exports.registerUser = (data) => {
  let fName = data.firstName;
  let lName = data.lastName;
  let email = data.email;
  let password1= data.password1;
  let password2= data.password2;
  let newUser = new User({
    firstName: fName,
    lastName: lName,
    email: email,
    password1: bcrypt.hashSync(password1, salt1),
    password2: bcrypt.hashSync(password2, salt2)
  }); 
  return newUser.save().then((user, rejected) => {
    if (user) {
      // return user; 
      return ({ message: "Successfull" });
    } else {
      // return 'Failed to Register a new account'; 
      return ({ message: "Unsuccessfull" });
    }; 
  });
};



// [SECTION] LOGIN
module.exports.loginUser = (req, res) => {
      // console.log(req.body)
      User.findOne({email: req.body.email})
      .then(foundUser => {
    
        if(foundUser === null){
          return res.send({ message: "User Not Found" });
        } else {
          const isPasswordCorrect = bcrypt.compareSync(req.body.password1, foundUser.password1)
          console.log(isPasswordCorrect)
          if(isPasswordCorrect){
            return res.send({accessToken: auth.createAccessToken(foundUser)})
          } else {
            return res.send(false);
          }
        }
      })
      .catch(err => res.send(err))
    };

// [SECTION] Functionalities [RETREIVE] [Authenticated user]
module.exports.getUserDetails = (req, res) => {
    // console.log(req.user)
    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))
  };
// [SECTION] Functionalities [RETREIVE] [Get all users]
module.exports.getAllUsers = (req, res) => {
  return User.find({}).then(result => {
      return res.send(result);
  });
};

// [SECTION] Functionality [UPDATE]
module.exports.updateUser  = (id, details) => {
  let fName = details.firstName;
  let lName = details.lastName;
  let email = details.email;
  let passW = details.password; 

  let updateUser = {
      fname: fName, 
      lname: lName,
      email: email,
      password: bcrypt.hashSync(passW, salt)
  }

  return User.findByIdAndUpdate(id, details).then((userUpdated, err) => {
      if (err) {
          return 'Failed to update User';
      }else{
          return 'Successfully Updated User';
      }
  }); 
};

// [SECTION] Functionality [UPDATE] [SET USER AS AN ADMIN]
module.exports.setAsAdmin = (id) => {
  let updates = {
      isAdmin: true
  }
  return User.findByIdAndUpdate(id, updates).then((setAsAdmin, error) => {
      if (setAsAdmin) {
          return `The User ${id} has been updated as admin`;
      }else{
          return `Failed to set the user as admin`;
      };
  });
};

// [SECTION] Functionality [UPDATE] [SET ADMIN AS AN USER]
module.exports.setAsUser = (id) => {
  let updates = {
      isAdmin: false
  }
  return User.findByIdAndUpdate(id, updates).then((setAsUser, error) => {
      if (setAsUser) {
          return `The User ${id} has been updated as a normal user`;
      }else{
          return `Failed to set the user as a normal user`;
      };
  });
};

// [SECTION] Functionalities [DELETE]
module.exports.deleteUser = (id) => {
  return User.findByIdAndRemove(id).then((removeUser, err) => {
  if (err) {
      return 'No User Was Removed';
  }else{
      return 'User Sucessfully Deleted'
  }
  });
};