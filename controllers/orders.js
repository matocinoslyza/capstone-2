// [SECTION] Dependencies and Modules
const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');

// [SECTION] [ORDER]
    module.exports.order = (req, res) => {
    console.log(req.body.productId)
    console.log(req.user.id)
    let orderProductId = req.body.productId;
    let orderProductQuantity = req.body.quantity;
    let orderByUser = req.user.id;

    return Product.findById(orderProductId).then((foundExistingProduct) => {
        if (foundExistingProduct) {
          let orderAmount = foundExistingProduct.price * orderProductQuantity;
          
          let newOrder = new Order({
            totalAmount: orderAmount,
            productId: orderProductId,
            quantity: orderProductQuantity,
            customerId: orderByUser,
            orders: [
              {
                productId: orderProductId,
                customerId: orderByUser,
                quantity: orderProductQuantity,
                totalAmount: orderAmount
              },
            ],
          });
          return newOrder.save().then((SaveSuccess, SaveErr) => {
            if (SaveErr) {
              return "Failed to Save Document";
            } else {
              let orderID = SaveSuccess._id;
              console.log(orderID)
              let updateUser = {
                orders: [
                  {
                    orderID: orderID,
                    productId: orderProductId,
                    customerId: orderByUser,
                    quantity: orderProductQuantity,
                    totalAmount: orderAmount
                  },
                ],
              };
              return User.findByIdAndUpdate(orderByUser, {$push : updateUser}).then(
                (foundExistingUser) => {
                    console.log(foundExistingUser)
                  if (foundExistingUser) {
                return res.send(`Order Successful`)
                  } else {
                    return `error in updating user`;
                  }
                }
              );
               SaveSuccess;
            }
          });
        }
    })
    }


// GET ORDERS [AUTHENTICATED]
    module.exports.getOrders = (req, res) => {
      // console.log(req.user.id)
      User.findById(req.user.id).then(result => res.send(result.orders))
      .catch(err => res.send(err))
    }

  // [SECTION] Functionalities [RETREIVE]

module.exports.getAllOrders = (req, res) => {
  return Order.find({}).then(resultOftheQuery => {
    // console.log(resultOftheQuery);  
    return res.send(resultOftheQuery);
  });
};