// [SECTION] Dependencies and Modules
const Product = require('../models/Product');

// [SECTION] Functionality [CREATE]
module.exports.createProduct = (info) => {
    let pName = info.name;
    let pDesc = info.description;
    let pCost = info.price;

    let newProduct = new Product({
        name: pName,
        description: pDesc,
        price:  pCost
    })

    return newProduct.save().then((savedProduct, error) => {
        if (error) {
            return false;
        }else{
    
            return savedProduct;
        };
    });
};
// [SECTION] Functionality [RETRIEVE] [GET ALL PRODUCT] [ADMIN]
module.exports.getAllProduct = () => {
    return Product.find({}).then(result => {
        return result;
    });
};

// // [SECTION] Functionality [RETRIEVE] [GET ALL PRODUCT] [USER]
// module.exports.getAllProductAsUser = () => {
//     return Product.find({}).then(result => {
//         return result;
//     });
// };

// // [SECTION] Functionality [RETRIEVE] [GET ALL PRODUCT] [GUEST]
// module.exports.getAllProductAsGuest = () => {
//     return Product.find({}).then(result => {
//         return result;
//     });
// };

// Retrieve a single product
module.exports.getProduct = (id) => {
    return Product.findById(id).then(resultOfQuery => {
        return resultOfQuery;
    });
};
// Retrieve all active product
module.exports.getAllActiveProduct = () => {
    return Product.find({isActive: true}).then(resultOftheQuery => {
        return resultOftheQuery;
    });
};

// [SECTION] Functionality [UPDATE]
module.exports.updateProduct  = (id, details) => {
    let pName = details.name;
    let pDesc = details.description;
    let pCost = details.price;

    let updatedProduct = {
        name: pName, 
        description: pDesc,
        price: pCost
    }

    return Product.findByIdAndUpdate(id, details).then((productUpdated, err) => {
        if (err) {
            return 'Failed to update Product';
        }else{
            return 'Successfully Updated Product';
        }
    }); 
};

// Deactivate Product
module.exports.deactiveateProduct = (id) => {
    let updates = {
        isActive: false
    }
    return Product.findByIdAndUpdate(id, updates).then((archived, error) => {
        if (archived) {
            return `The Product ${id} has been deactivated`;
        }else{
            return `Failed to archive product`;
        };
    });
};
// Reactivate Product
module.exports.reactivateProduct = (id) => {
    let updates = {
        isActive: true
    }
    return Product.findByIdAndUpdate(id, updates).then((reactivate, error) => {
        if (reactivate) {
            return `The Product ${id} has been reactivated`;
        }else{
            return `Failed to reactivate product`;
        };
    });
};

// [SECTION] Functionality [DELETE/ARCHIVE]
module.exports.deleteProduct = (id) => {
    return Product.findByIdAndRemove(id).then((removeProduct, err) => {
    if (err) {
        return 'No Product Was Removed';
    }else{
        return 'Product Sucessfully Deleted'
    }
    });

};


