// [SECTION] Dependencies and Modules
const mongoose = require('mongoose')

// [SECTION] Blueprint Schema
const userSchema = new mongoose.Schema({
    firstName:{
        type: String,
        required: [true, 'First Name is Required']
    },
    lastName:{
        type: String,
        required: [true, 'Last Name is Required']
    },
    email:{
        type: String,
        required: [true, 'Email is Required']
    },
    password1:{
        type: String,
        required: [true, 'Password is Required']
    },
    password2:{
        type: String,
        required: [true, 'Password is Required']
    },
    isAdmin:{
        type: Boolean,
        default: false
    },
    orders: [
        {
            productId: {
                type: String,
                required: [true, "Product ID is Required"]
            },
            customerId: {
                type: String,
                required: [true, "Product ID is Required"]
            },
            quantity: {
                type: Number,
                default: new Number()
            },
            totalAmount: {
                type: Number,
                default: new Number()
            },
            orderedOn: {
              type: String,
              default: new Date()
          },
            status: {
                type: String,
                default: "Ordered"
            }
        }
    ]
});

// [SECTION] Model
const User = mongoose.model('User', userSchema)
module.exports = User; 