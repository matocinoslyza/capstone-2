//[SECTION] Dependencies and Modules
const mongoose = require("mongoose"); 

//[SECTION] Schema / Document Blueprint
  const orderSchema = new mongoose.Schema({
	productId: {
		type: String,
		required: [true, 'Product Id is Required']
	},
	customerId: {
		type: String,
		required: [true, 'Product Id is Required']
	},
	quantity: {
		type: Number,
		required: [true, 'Quantity is Required']
	},
	totalAmount: {
  		type: Number,
  		default: new Number()
  	},
  	orderedOn: {
  		type: Date,
  		default: new Date()
  	},
  });


//[SECTION] Model
   const Order = mongoose.model("Order", orderSchema);
   module.exports = Order; 
